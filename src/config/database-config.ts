export interface DatabaseConfig {
    username: string;
    password: string;
    database: string;
    host: string;
    port: number;
    dialect: string;
    logging: boolean | Function;
    force: boolean;
    timezone: string;
    modelPaths: string[];
}

export const databaseConfig: DatabaseConfig = {
    username: "orchard_int",
    password: "orchard_int",
    database: "orchard_satellite_main_int",
    host: "int-postgres.jmango360.com",
    port: 5432,
    dialect: "postgres",
    modelPaths: [__dirname + "src/entities"],
    logging: true,
    force: false,
    timezone: "+00:00"
};