import { Request, Response } from "express";
import { Device } from "../entities/Device";

/**
 * GET /
 * Home page.
 */
export let index = (req: Request, res: Response) => {
    const device = Device.findById("cc7ca89d-de1c-4e6b-840c-f662d06c50ec");
    console.log("DEVICE: " + JSON.stringify(device));
  res.render("home", {
    title: "Home"
  });
};
