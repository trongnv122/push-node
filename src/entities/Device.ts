import { Table, Column, Model , DataType } from "sequelize-typescript";

@Table
export class Device extends Model<Device> {
    @Column(DataType.UUID)
    uniqueId: string;
    @Column(DataType.TEXT)
    referenceId: string;
    @Column(DataType.TEXT)
    applicationReferenceId: string;
    @Column(DataType.TEXT)
    notificationId: string;
    @Column(DataType.INTEGER)
    status: number;
    @Column(DataType.TEXT)
    firebaseSenderId: string;
    @Column(DataType.TEXT)
    firebaseServerKey: string;
    @Column(DataType.INTEGER)
    osType: number;
}