import mongoose from "mongoose";

export type Message = mongoose.Document & {
    type: string,
    status: string,
    productDetails: any,
    catalogDetails: any,
    users: string[],
    groups: string[],
    title: string,
    content: string,

    // comparePassword: comparePasswordFunction,
    // gravatar: (size: number) => string
};

const messageSchema = new mongoose.Schema({
    type: String,
    status: String,
    productDetails: Object,
    catalogDetails: Object,
    users: [],
    groups: [],
    title: String,
    content: String,
}, { timestamps: true });
